terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
  token     = var.token
}

data "yandex_compute_image" "ubuntu_image" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "prod" {

  name        = "vm-prod"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.image_id
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat       = true
    ip_address = "192.168.10.80"
  }

  metadata = {
    user-data = "${file("./user_data")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.vm-test.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ansible"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ansible -i '${yandex_compute_instance.prod.network_interface.0.nat_ip_address},' --private-key ~/.ssh/id_rsa -T 300 ansible/docker.yml" 
  }
}

resource "yandex_compute_instance" "vm-test" {

  name        = "linux-vm"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.image_id
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat       = true
    ip_address = "192.168.10.20"
  }

  metadata = {
    user-data = "${file("./user_data")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.vm-test.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ansible"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ansible -i '${yandex_compute_instance.vm-test.network_interface.0.nat_ip_address},' --private-key ~/.ssh/id_rsa -T 300 ansible/docker.yml" 
  }
}

resource "yandex_compute_instance" "gitlab-runner" {

  name        = "runner-vm"
  platform_id = "standard-v1"
  zone        = var.zone
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.image_id
      size = 10
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    ip_address = "192.168.10.10"
    nat       = true
  }

  metadata = {
    user-data = "${file("./user_data")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ansible"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ansible -i '${yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address},' --private-key ~/.ssh/id_rsa -T 300 ansible/gitlab-runner.yml" 
  }
}

resource "yandex_compute_instance" "monitoring" {

  name        = "monitoring-vm"
  platform_id = "standard-v1"
  zone        = var.zone
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.image_id
      size = 10
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    ip_address = "192.168.10.15"
    nat       = true
  }

  metadata = {
    user-data = "${file("./user_data")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ansible"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ansible -i '${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address},' --private-key ~/.ssh/id_rsa -T 300 ansible/monitoring.yml" 
  }
}

resource "yandex_compute_instance" "grafana" {

  name        = "grafana-vm"
  platform_id = "standard-v1"
  zone        = var.zone
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.image_id
      size = 10
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    ip_address = "192.168.10.16"
    nat       = true
  }

  metadata = {
    user-data = "${file("./user_data")}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.grafana.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ansible"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ansible -i '${yandex_compute_instance.grafana.network_interface.0.nat_ip_address},' --private-key ~/.ssh/id_rsa -T 300 ansible/grafana.yml" 
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"

  target {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    address   = yandex_compute_instance.prod.network_interface.0.ip_address
  }
}

resource "yandex_lb_network_load_balancer" "lb-web" {
  name = "lb-web"

  listener {
    name = "listener-web-servers"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}

resource "yandex_dns_zone" "zone1" {
  name        = "my-zone"
  description = "Public zone"

  labels = {
    label1 = "public"
  }
  zone    = var.domain
  public  = true
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = var.domain
  type    = "A"
  ttl     = 200
  data    = [[for s in yandex_lb_network_load_balancer.lb-web.listener: s.external_address_spec.*.address].0[0]]
}

resource "yandex_dns_recordset" "test" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "test.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-test.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "runner" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "runner.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "monitoring" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "monitoring.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "grafana" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "grafana.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.grafana.network_interface.0.nat_ip_address]
}

output "internal_ip_address_prod" {
  value = yandex_compute_instance.prod.network_interface.0.ip_address
}

output "external_ip_address_prod" {
  value = yandex_compute_instance.prod.network_interface.0.nat_ip_address
}

output "internal_ip_address_vm-test" {
  value = yandex_compute_instance.vm-test.network_interface.0.ip_address
}

output "external_ip_address_vm-test" {
  value = yandex_compute_instance.vm-test.network_interface.0.nat_ip_address
}

output "internal_ip_address_gitlab-runner" {
  value = yandex_compute_instance.gitlab-runner.network_interface.0.ip_address
}

output "external_ip_address_gitlab-runner" {
  value = yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address
}

output "internal_ip_address_monitoring" {
  value = yandex_compute_instance.monitoring.network_interface.0.ip_address
}

output "external_ip_address_monitoring" {
  value = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
}

output "internal_ip_address_grafana" {
  value = yandex_compute_instance.grafana.network_interface.0.ip_address
}

output "external_ip_address_grafana" {
  value = yandex_compute_instance.grafana.network_interface.0.nat_ip_address
}

output "lb_ip_address" {
  value = [for s in yandex_lb_network_load_balancer.lb-web.listener: s.external_address_spec.*.address].0[0]
}